import os


def index():
    search = request.vars['query']
    path = os.path.dirname("..\\web2py\\applications\\Downloads\\static\\files\\")
    files = []
    d = os.listdir(path)
    for fil in d:
        fileType = fil[-3:]
        if fileType == 'exe' or fileType == 'msi' or fileType == 'zip':
            txt = open("..\\web2py\\applications\\Downloads\\static\\files_data\\"+fil[:-3]+"txt")
            size = os.path.getsize("..\\web2py\\applications\\Downloads\\static\\files\\"+fil)
            description = txt.readline()
            fileName = fil[:-4]
            fileName = fileName.replace('_',' ',3)
            fi = {
                'image': 'files_data/'+fil[:-3]+'png',
                'name': fileName, 'description': description,
                'location': 'files/'+fil,
                'size': sizeConvert(size)
                }
            if search:
                if str(fileName).lower().find(search) != -1 or str(description).lower().find(search) != -1:
                    files.append(fi)
            else:
                files.append(fi)
    return dict(files=files, path=path, query=search)



@cache.action()
def download():
    return response.download(request, db)


def call():
    return service()


def sizeConvert(byte):
    byte = float(byte)
    if byte >= 1024:
        kiloBytes = byte/1024
        if kiloBytes >= 1024:
            megaBytes = kiloBytes/1024
            if megaBytes >= 1024:
                gigaBytes = megaBytes/1024
                return str(round(gigaBytes, 2)) + " GB"
            else:
                return str(round(megaBytes, 2)) + " MB"
        else:
            return str(round(kiloBytes, 2)) + " KB"
    else:
        return str(byte) + " Bytes"
